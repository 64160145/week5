package com.watsapon.week5;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import com.SelectionSortApp;
import org.junit.Test;
public class SelectionSortAppTest {
    @Test
    public void shouldFindMinIndexCase1(){
        int arr[] = {5,4,3,2,1};
        int pos = 0;
        int minIndex = SelectionSortApp.FindMinIndex(arr,pos);
        assertEquals(4, minIndex);
    }
    @Test
    public void shouldFindMinIndexCase2(){
        int arr[] = {1,4,3,2,5};
        int pos = 1;
        int minIndex = SelectionSortApp.FindMinIndex(arr,pos);
        assertEquals(3, minIndex);
    }
    @Test
    public void shouldFindMinIndexCase3(){
        int arr[] = {1,2,3,4,5};
        int pos = 2;
        int minIndex = SelectionSortApp.FindMinIndex(arr,pos);
        assertEquals(2, minIndex);
    }
    @Test
    public void shouldFindMinIndexCase4(){
        int arr[] = {1,1,1,1,1,0,1,1};
        int pos = 0;
        int minIndex = SelectionSortApp.FindMinIndex(arr,pos);
        assertEquals(5, minIndex);
    }
    @Test
    public void shouldSwapMinIndexCase1(){
        int arr[] = {5,4,3,2,1};
        int expected[] = {1,4,3,2,5};
        int first = 0;
        int second = 4;
        SelectionSortApp.swap(arr,first,second);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void shouldSwapMinIndexCase2(){
        int arr[] = {5,4,3,2,1};
        int expected[] = {5,4,3,2,1};
        int first = 0;
        int second = 0;
        SelectionSortApp.swap(arr,first,second);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void shouldSelectionSortTestCase1(){
        int arr[] = {5,4,3,2,1};
        int sortArr[] = {1,2,3,4,5};
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortArr, arr);
    }
    @Test
    public void shouldSelectionSortTestCase2(){
        int arr[] = {10,9,8,7,6,5,4,3,2,1};
        int sortArr[] = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortArr, arr);
    }
    @Test
    public void shouldSelectionSortTestCase3(){
        int arr[] = {5,9,3,7,6,10,4,8,2,1};
        int sortArr[] = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortArr, arr);
    }
}